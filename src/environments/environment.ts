// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBf5bBLq01BFZQouj49n_zCKl3nCIDBFJ0",
    authDomain: "oshop-21cde.firebaseapp.com",
    databaseURL: "https://oshop-21cde.firebaseio.com",
    projectId: "oshop-21cde",
    storageBucket: "oshop-21cde.appspot.com",
    messagingSenderId: "1040239151293",
    appId: "1:1040239151293:web:8ccc3eb392fb7929cecfd4",
    measurementId: "G-EG7H4LDH9P"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
